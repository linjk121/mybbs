create database mybbs;

use mybbs;

create table article (
	id int primary key auto_increment,
	pid int,
	rootid int,
	title varchar(255),
	cont text,
	pdate datetime,
	isleaf int 
);

insert into article values (null, 0, 1, '产品经理', '产品经理', now(), 1);
insert into article values (null, 1, 1, '苹果电脑', '苹果电脑',now(), 1);
insert into article values (null, 2, 1, '广东湛江', '广东湛江', now(), 0);
insert into article values (null, 2, 1, '广佛地铁', '广佛地铁', now(), 1);
insert into article values (null, 4, 1, '佛山南海', '佛山南海', now(), 0);
insert into article values (null, 1, 1, '南海桂城', '南海桂城', now(), 1);
insert into article values (null, 6, 1, '项目管理', '项目管理', now(), 0);
insert into article values (null, 6, 1, '智能电表', '智能电表', now(), 0);
insert into article values (null, 2, 1, '小米手机', '小米手机', now(), 1);
insert into article values (null, 9, 1, '滴滴打车', '滴滴打车', now(), 0);
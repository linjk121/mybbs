<%@ page language="java" contentType="text/html; charset=gbk"
    pageEncoding="gbk"%>
<%@ page import="java.sql.*" %>

<!-- <jsp:useBean id="admin" class="String" scope="session"/> -->

<%!
	private void delete(Connection conn, int id){
	Statement stmt   = null;
	ResultSet rs     = null;
	
	try{
		stmt = conn.createStatement();
		String sql = "select * from article where pid = " + id;
		rs = stmt.executeQuery(sql);
		while(rs.next()){
			delete(conn, rs.getInt("id"));
		}
		stmt.executeUpdate("delete from article where id=" + id);
	}
	catch (SQLException e){
		e.printStackTrace();
	}
	finally{
		try{
			if (rs != null){
				rs.close();
				rs = null;
			}
			if (stmt != null){
				stmt.close();
				stmt = null;
			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	}
%>

<%
	String admin = (String)session.getAttribute("admin");
	
	if (admin == null && !admin.equals("true")){
		out.println("请先登录");
		return;
	}

	String strId = request.getParameter("id");
	int id = Integer.parseInt(strId);
	String strpId = request.getParameter("pid");
	int pid = Integer.parseInt(strpId);
	
	Class.forName("com.mysql.jdbc.Driver");
	String url = "jdbc:mysql://172.16.143.183:3306/bbs?user=root&password=shenzhennuli8";
	Connection conn = DriverManager.getConnection(url);
	
	conn.setAutoCommit(false);
	
	delete(conn, id);
	//判断父结点是否还有叶子结点
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery("select count(*) from article where pid = " + pid);
	rs.next();
	int count = rs.getInt(1);
	rs.close();
	stmt.close();
	if (count <= 0){//父结点没有子结点了
		Statement stm = conn.createStatement();
		stm.executeUpdate("Update article set isleaf = 0 where id = " + pid);
		stm.close();
	}
	
	conn.commit();
	conn.setAutoCommit(true);
	
	conn.close();
	
	response.sendRedirect("ShowArticleTree.jsp");
%>    